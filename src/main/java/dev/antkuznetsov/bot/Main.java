package dev.antkuznetsov.bot;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Location;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.GetUpdates;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.GetUpdatesResponse;
import dev.antkuznetsov.bot.model.Weather;

import java.io.IOException;
import java.util.List;

public class Main {
    private static final String TELEGRAM_TOKEN = "1387943351:AAHB8qGT62oXGJYA1ynkyDq2dvArlWM9Eu4";
    private static final String YANDEX_TOKEN = "75f2a748-a4c5-480a-938d-e38fa2da5947";
    private static int offset = 0;

    public static void main(String[] args) throws IOException {
        TelegramBot bot = new TelegramBot(TELEGRAM_TOKEN);
        WeatherService weatherService = new WeatherService(YANDEX_TOKEN);
        while (true) {
            GetUpdates getUpdates = new GetUpdates().limit(1).offset(offset).timeout(0);
            GetUpdatesResponse updatesResponse = bot.execute(getUpdates);

            List<Update> updates = updatesResponse.updates();

            if (!updates.isEmpty()) {
                Update update = updates.get(0);

                System.out.println(update);

                Message message = update.message();

                String answer = "";

                if ("/start".equals(message.text())) {
                    answer = "Привет! Я могу показать прогноз погоды.";
                } else if (message.location() != null) {
                    Location location = message.location();
                    Weather weather = weatherService.getWeather(location.latitude(), location.longitude());
                    answer = "Погода на сегодня: \n" +
                    "Температура: " + weather.getFact().getTemp() + "\n" +
                    "Ощущается как : " + weather.getFact().getFeels_like() + "\n" +
                    "Скорость ветра: " + weather.getFact().getWind_speed() + "\n" +
                    "Давление: " + weather.getFact().getPressure_mm() + "\n" +
                    "Подробнее: " + weather.getInfo().getUrl() + "\n";
                } else {
                    answer = "Я пока не знаю такой команды :(";
                }
                SendMessage sendMessage = new SendMessage(message.chat().id(), answer);

                bot.execute(sendMessage);

                offset = update.updateId() + 1;
            }
        }
    }
}